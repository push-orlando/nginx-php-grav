FROM phusion/baseimage:0.9.22

MAINTAINER Push Dev <dev@pushhere.com>

CMD ["/sbin/my_init"]

ENV HOME /root
ENV DEBIAN_FRONTEND noninteractive

# Install core packages
RUN apt-get update -q
RUN apt-get upgrade -y -q
RUN apt-get install -y -q build-essential \
    ca-certificates \
    git-core \
    php \
    php-apcu \
    php-cli \
    php-curl \
    php-fpm \
    php-gd \
    php-mbstring \
    php-xml \
    php-zip \
    python2.7 \
    nginx

# Install NodeJS and NPM
# JPM: Borrowed from official NodeJS docker image (joyent/docker-node)
# verify gpg and sha256: http://nodejs.org/dist/v0.10.36/SHASUMS256.txt.asc
# Node.js releases are signed with one of the following GPG keys:
# Current keys can be found here : https://github.com/nodejs/node
RUN gpg --keyserver pool.sks-keyservers.net --recv-keys 94AE36675C464D64BAFA68DD7434390BDBE9B9C5 FD3A5288F042B6850C66B31F09FE44734EB7990E \
    71DCFD284A79C3B38668286BC97EC7A07EDE3FC1 DD8F2338BAE7501E3DD5AC78C273792F7D83545D C4F0DFFF4E8C1A8236409D08E73BC641CC11F4C8 \
    B9AE9905FFD7803F25714661B63B535A4C206CA9 56730D5401028683275BD23C23EFEFE93C4CFFFE 77984A986EBC2AA786BC0F66B01FBB92821C587A

ENV NODE_VERSION 9.2.0
# Updating NPM to a specific version throws semver error
# ENV NPM_VERSION 5.5.1

RUN curl -SLO "http://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.gz"
RUN curl -SLO "http://nodejs.org/dist/v$NODE_VERSION/SHASUMS256.txt.asc"
RUN gpg --verify SHASUMS256.txt.asc
RUN grep " node-v$NODE_VERSION-linux-x64.tar.gz\$" SHASUMS256.txt.asc | sha256sum -c -
RUN tar -xzf "node-v$NODE_VERSION-linux-x64.tar.gz" -C /usr/local --strip-components=1
RUN rm "node-v$NODE_VERSION-linux-x64.tar.gz" SHASUMS256.txt.asc
# Updating NPM to a specific version throws semver error
#RUN npm install -g npm@"$NPM_VERSION"
#module.js:544
#    throw err;
#    ^
#
#Error: Cannot find module 'semver'
#    at Function.Module._resolveFilename (module.js:542:15)
#    at Function.Module._load (module.js:472:25)
#    at Module.require (module.js:585:17)
#    at require (internal/module.js:11:18)
#    at Object.<anonymous> (/usr/local/lib/node_modules/npm/lib/utils/unsupported.js:2:14)
#    at Module._compile (module.js:641:30)
#    at Object.Module._extensions..js (module.js:652:10)
#    at Module.load (module.js:560:32)
#    at tryModuleLoad (module.js:503:12)
#    at Function.Module._load (module.js:495:3)
#The command '/bin/sh -c npm cache verify' returned a non-zero code: 1
RUN npm cache verify

# Verify Node & NPM installs
RUN node -v
RUN npm -v

# Set default python version for node (required for node-sass)
RUN npm config set python $(which python2.7)

# Install yarn via npm
RUN npm install -g yarn

# Clean up apt packages
RUN apt-get clean -q && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Get Grav
RUN rm -fR /usr/share/nginx/html/
RUN git clone https://github.com/getgrav/grav.git /usr/share/nginx/html/

#Install Grav
WORKDIR /usr/share/nginx/html/
RUN bin/composer.phar self-update
RUN bin/grav install
RUN chown -R www-data:www-data *
RUN find . -type f | xargs chmod 664
RUN find . -type d | xargs chmod 775
RUN find . -type d | xargs chmod +s
RUN umask 0002

# Configure Nginx - enable gzip
RUN sed -i 's|# gzip_types|  gzip_types|' /etc/nginx/nginx.conf

# Setup Grav configuration for Nginx
RUN touch /etc/nginx/grav_conf.sh
RUN chmod +x /etc/nginx/grav_conf.sh
RUN echo '#!/bin/bash \n\
    echo "" > /etc/nginx/sites-available/default \n\
    ok="0" \n\
    while IFS="" read line \n\
    do \n\
        if [ "$line" = "server {" ]; then \n\
            ok="1" \n\
        fi \n\
        if [ "$ok" = "1" ]; then \n\
            echo "$line" >> /etc/nginx/sites-available/default \n\
        fi \n\
        if [ "$line" = "}" ]; then \n\
            ok="0" \n\
        fi \n\
    done < /usr/share/nginx/html/webserver-configs/nginx.conf' >> /etc/nginx/grav_conf.sh
RUN /etc/nginx/grav_conf.sh
RUN sed -i \
        -e 's|root /home/USER/www/html|root   /usr/share/nginx/html|' \
        -e 's|unix:/var/run/php5-fpm.sock;|unix:/run/php/php7.0-fpm.sock;|' \
    /etc/nginx/sites-available/default

# Setup Php service
RUN mkdir -p /run/php/
RUN touch /run/php/php7.0-fpm.sock
RUN mkdir -p /etc/service/php-fpm
RUN touch /etc/service/php-fpm/run
RUN chmod +x /etc/service/php-fpm/run
RUN echo '#!/bin/bash \n\
    exec /usr/sbin/php-fpm7.0 -F' >> /etc/service/php-fpm/run

# Setup Nginx service
RUN mkdir -p /etc/service/nginx
RUN touch /etc/service/nginx/run
RUN chmod +x /etc/service/nginx/run
RUN echo '#!/bin/bash \n\
    exec /usr/sbin/nginx -g "daemon off;"' >>  /etc/service/nginx/run

# Setup SSH service
RUN sed -i \
        -e 's|#PasswordAuthentication no|PasswordAuthentication no|' \
        -e 's|#UsePAM yes|UsePAM no|' \
    /etc/ssh/sshd_config
RUN rm -f /etc/service/sshd/down
RUN /etc/my_init.d/00_regen_ssh_host_keys.sh

# Expose configuration and content volumes
VOLUME /root/.ssh/ /etc/nginx/ /usr/share/nginx/html/

# Public ports
EXPOSE 80 22